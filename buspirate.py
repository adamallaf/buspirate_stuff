import serial


class BusPirate:
    def __init__(self, port="/dev/BusPirate"):
        self.__port = port
        self.__serial = serial.Serial(
            port=self.__port, baudrate=115200, timeout=0
        )

    def __del__(self):
        self.close()

    def close(self):
        self.__serial.close()

    def reset(self):
        self.__serial.write("#\n")

    def start_i2c(self, frequency=3):
        self.__serial.write("m4\n")
        self.__serial.write("{}\n".format(frequency))

    def start_uart(self, baudrate=3):
        self.__serial.write("m3\n")

    def write(self, data):
        self.__serial.write("{}\n")
