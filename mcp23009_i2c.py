import serial
import time


mcp23009 = 0x24


def i2c_write(ser, slave_addr, reg, data):
    ser.write("[ {} {} {} ]\n".format((slave_addr << 1), reg, data))


def counter(ser, seconds=1, delay=1):
    n = 0xFF
    for i in range(int(seconds / delay) + 1):
        i2c_write(ser, mcp23009, 0, n)
        n = (n - 1) & 0xFF
        time.sleep(delay)


def dot_slide(ser, seconds=1, delay=1):
    n = 1
    shift_d = 0
    for i in range(int(seconds / delay) + 1):
        if n == 0x80:
            shift_d = 1
        if n == 1:
            shift_d = 0
        if shift_d:
            n = (n >> 1)
        else:
            n = (n << 1)
        i2c_write(ser, mcp23009, 0, 0xff ^ n)
        time.sleep(delay)


def main():
    s = serial.Serial(port="/dev/BusPirate" ,baudrate=115200, timeout=0)
    s.write("m4\n")
    time.sleep(0.1)
    s.write("3\n")
    time.sleep(0.1)
    s.write("W\n")
    time.sleep(0.1)
    counter(s, 10, 0.2)
    dot_slide(s, 10, 0.025)
    s.write("w\n")
    time.sleep(0.1)
    s.write("m1\n")
    time.sleep(0.1)
    s.close()


if __name__ == "__main__":
    main()
